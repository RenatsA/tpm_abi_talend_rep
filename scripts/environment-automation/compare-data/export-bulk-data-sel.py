import os
import sys
import urllib
import urllib2
import json
import pprint
import requests
import logging
import collections
import getopt
import fnmatch
import re
import datetime
import argparse
import time
from xml.etree import ElementTree
import codecs
import base64

sys.path.insert(0, '../../libs')
import SoapUtils

apiVersion = "41.0"

class BulkJob():
    def __init__(self,objectName,query):
        self.objectName=objectName
        self.query=query

def exportData():
    bulkOperation = "query"
    global bulkJob

    for bulkJob in bulkJobs:
        logger.info("Exporting data from = " + bulkJob.objectName)
        logger.info("Sending request to create Bulk API Job.")

        # create the Bulk API Job
        response = SoapUtils.createBulkJob(bulkOperation,bulkJob.objectName)
        asyncProcessState=""
        asyncProcessId=""
        asyncBatchJobId=""

        
        if(response.status_code == 201):
            for item in ElementTree.fromstring(response.text.encode('utf-8').strip()).iter(tag='{http://www.force.com/2009/06/asyncapi/dataload}id'):
                asyncProcessId=item.text.encode('utf-8').strip()
            for item in ElementTree.fromstring(response.text.encode('utf-8').strip()).iter(tag='{http://www.force.com/2009/06/asyncapi/dataload}state'):
                asyncProcessState=item.text.encode('utf-8').strip()


            if(asyncProcessId is not None and asyncProcessState is not None):
                logger.info("asyncProcessId = " + asyncProcessId)
                logger.info("asyncProcessState = " + asyncProcessState)

                # add batch to job
                logger.info("Sending request to add Batch to Bulk Job.")
                logger.debug("Request data: Query = " + bulkJob.query)
                response = SoapUtils.addBatchToBulkJob(asyncProcessId,bulkJob.query)

                if(response.status_code == 201):
                    for item in ElementTree.fromstring(response.text.encode('utf-8').strip()).iter(tag='{http://www.force.com/2009/06/asyncapi/dataload}id'):
                        asyncBatchJobId=item.text.encode('utf-8').strip()
                    
                    if(asyncBatchJobId is not None):
                        trackJobStatus(asyncProcessId,asyncBatchJobId)
                    else:
                        logger.error("Failed to extract asyncJobId")
                        logger.error(response.text)
                        sys.exit(1)

            else:
                logger.error("Failed to extract asyncProcessId or asyncProcessState")
                logger.error(response.text)
                sys.exit(1)


        else:
            logger.error("Request failed.")
            logger.error(response.text)
            sys.exit(1)

def trackJobStatus(jobId,batchJobId):
    logger.info("Checking Status for Job Id = " + jobId + " and Batch Id = " + batchJobId)

    time.sleep(5)
    response = SoapUtils.checkBulkJobStatus(jobId,batchJobId)

    asyncJobStatus=""

    if(response.status_code == 200):
        for item in ElementTree.fromstring(response.text.encode('utf-8').strip()).iter(tag='{http://www.force.com/2009/06/asyncapi/dataload}state'):
            asyncJobStatus=item.text.encode('utf-8').strip()
            

        
        if(asyncJobStatus == "Open" or asyncJobStatus == "Queued" or asyncJobStatus == "InProgress"):
            logger.info("Status = " + asyncJobStatus)
            trackJobStatus(jobId,batchJobId)

        if(asyncJobStatus == "Aborted"):
            logger.error("Status = " + asyncJobStatus)
            logger.error(response.text)
            sys.exit(1)
        
        
        if(asyncJobStatus == "Failed"):
            logger.error("Status = " + asyncJobStatus)
            logger.error(response.text)
            sys.exit(1)

        if(asyncJobStatus == "Completed"):
            logger.info("Status = " + asyncJobStatus)
            # fetch results
            for item in ElementTree.fromstring(response.text.encode('utf-8').strip()).iter(tag='{http://www.force.com/2009/06/asyncapi/dataload}numberRecordsProcessed'):
                logger.info("numberRecordsProcessed = " + str(item.text.encode('utf-8').strip()))
            for item in ElementTree.fromstring(response.text.encode('utf-8').strip()).iter(tag='{http://www.force.com/2009/06/asyncapi/dataload}numberRecordsFailed'):
                logger.info("numberRecordsFailed = " + str(item.text.encode('utf-8').strip()))
            
            # get the results
            getBulkResults(jobId,batchJobId)
        
        else:
            #logger.info("Attempt failed, unknown status type Status = " + asyncJobStatus)
            #sys.exit(1)
            trackJobStatus(jobId,batchJobId)

    else:
        logger.error("Failed to check status.")
        logger.error(response.text)
        sys.exit(1)

def getBulkResults(jobId,batchJobId):
    logger.info("Retrieving Bulk results.")
    response = SoapUtils.getBulkJobResultId(jobId,batchJobId)
    
    if(response.status_code == 200):
        resultId=""
        for item in ElementTree.fromstring(response.text.encode('utf-8').strip()).iter(tag='{http://www.force.com/2009/06/asyncapi/dataload}result'):
            resultId = item.text.encode('utf-8').strip()
        
        if(resultId is not None):
            logger.info("Bulk result Id = " + resultId)
            # now send the request to get the results

            logger.info("Sending request to get Bulk result.")
            response = SoapUtils.getBulkJobResult(jobId,batchJobId,resultId)

            # export CSV to file
            if(response.status_code == 200):
                logger.info("Bulk Job result response is OK.")
                logger.info("Exporting to CSV file.")
                exportResults(response.text.encode('utf-8').strip())
            else:
                logger.error("Failed to get valid response.")
                logger.error(response.text)
                sys.exit(1)

            logger.info("Closing Bulk Job.")
            response = SoapUtils.closeBulkJob(jobId)
            closedState = ""

            for item in ElementTree.fromstring(response.text.encode('utf-8').strip()).iter(tag='{http://www.force.com/2009/06/asyncapi/dataload}state'):
                closedState = item.text.encode('utf-8').strip()
            
            if(closedState == "Closed"):
                logger.info("Bulk Job closed.")
            else:
                logger.warning("Failed to close Bulk Job")
                logger.warning("Bulk Job status = " + closedState)
            
        else:
            logger.error("Failed to extract Bulk result Id.")
            logger.error(response.text)
            sys.exit(1)    

def exportResults(data):
#    fileName = bulkJob.objectName + "-" + orgname + "-" + time.strftime("%Y%m%d-%H%M%S") + ".csv"
    fileName = time.strftime("%Y%m%d") + "-" + bulkJob.objectName + "-" + orgname + ".csv"

    with open(fileName, 'w') as csvfile:
        csvfile.write(data)
    
    csvfile.close()

    if(os.path.exists(fileName)):
        logger.info("Export successful. Filename = " + os.path.abspath(fileName))
    else:
        logger.error("Failed to export data.")
        sys.exit(1)

def readObjects():
    global bulkJobs
    bulkJobs=[]
    logger.info("Reading Objects to export into memory.")
    json_data = json.load(open(objects))

    if(json_data):
        if(table == "ALL"):
            for obj in json_data['Objects']['types']:
                bulkJob = BulkJob(obj['name'],obj['query'])
                bulkJobs.append(bulkJob)
        else:
            for obj in json_data['Objects']['types']:
                if(obj['name'] == table):
                    bulkJob = BulkJob(obj['name'],obj['query'])
                    logger.info("Selected table = " + bulkJob.objectName)
                    bulkJobs.append(bulkJob)
                    

        if(len(bulkJobs) > 0):
            logger.info("Objects to export stored in memory = " + str(len(bulkJobs)))
        else:
            logger.error("Failed to save list of Objects to export into memory.")
            sys.exit(1)
    else:
        logger.error("Failed to parse JSON data.")
        sys.exit(1)

def initialise_logger():
    	# create logger
	logger = logging.getLogger('logger')

	logger.setLevel(logging.DEBUG)
	# create console handler and set level to debug
	ch = logging.StreamHandler()
	ch.setLevel(logging.DEBUG)
	# create formatter
	formatter = logging.Formatter("%(asctime)s:%(levelname)s > %(message)s",
                              "%Y-%m-%d %H:%M:%S")
	# add formatter to ch
	ch.setFormatter(formatter)

	# add ch to logger
	logger.addHandler(ch)

	return logger
    
def main():
    global logger, username, password, org,objects, orgname, table
    logger=initialise_logger()

    logger.info("Exports Metadata from SF Org.")
    parser = argparse.ArgumentParser(description='Export Record data from SF Org.')
    parser.add_argument("--username", type=str, help='Specify the username to connect to the SF Org.')
    parser.add_argument("--password", type=str, help='Specify the password to connect to the SF Org.')
    parser.add_argument("--name", type=str, help='Specify the environment Id of the SF Org.')
    parser.add_argument("--org", type=str, help='Specify the link of the SF Org.')
    parser.add_argument("--file", type=str, help='Specify the export objects.json file.')
    parser.add_argument("--table", type=str, help='Specify the table to query.')
    
    args = parser.parse_args()

    if (args.username is not None and args.password is not None and args.name is not None and args.org is not None and args.file is not None and args.table is not None):
        username=args.username
        password=args.password
        org = args.org
        objects = args.file
        orgname = args.name
        table = args.table

        if(not os.path.exists(objects)):
            logger.error("Failed to locate " + objects)
            logger.error("Verify the path.")
            sys.exit(1)

        SoapUtils.buildEndpoints(org)
       
        logger.info("Authenticating.")
        logger.info("Server URL = " + org)
        logger.info("Username = " + username)

        readObjects()

        SoapUtils.login(username,password,org)
        
        if(SoapUtils.sessionId is not None and SoapUtils.serverUrl is not None and SoapUtils.metdataServerUrl is not None):
            logger.info("Authentication = OK")
			
            exportData()

            logger.info("Logging out.")
            SoapUtils.logout()

        else:
            logger.error("Authentication = FAILED")
            logger.error(SoapUtils.response.text)
           

    else:
        parser.print_help()
        sys.exit(1)

if __name__ == '__main__':
	main()
