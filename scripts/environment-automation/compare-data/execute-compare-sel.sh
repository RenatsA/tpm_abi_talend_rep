#!/bin/sh

if [ $# -eq 0 ]
	then
		echo "No arguments supplied"
elif [ $# -eq 12 ]
	then
		echo "Parameters Supplied"

		#Salesforce Credentials
		var_username_one=${1}
		var_password_one=${2}
		var_name_one=${3}
		var_org_one=${4}
		var_username_two=${5}
		var_password_two=${6}
		var_name_two=${7}
		var_org_two=${8}
		var_access_key=${9}
		var_secret_key=${10}
		var_table=${11}
		var_json_file=${12}
		
else
	echo "ERROR > Incorrect Parameters Supplied"
	exit 1
fi

python --version
python -W ignore export-bulk-data-sel.py --username ${var_username_two} --password ${var_password_two} --name ${var_name_two} --org ${var_org_two} --file ${var_json_file} --table ${var_table}
exit_code=$?
echo "Exist Code: ${exit_code}"
if [ ${exit_code} -ne 0 ]
then
	echo "Script error"
	exit 1
fi

python -W ignore export-bulk-data-sel.py --username ${var_username_one} --password ${var_password_one} --name ${var_name_one} --org ${var_org_one} --file ${var_json_file} --table ${var_table}
exit_code=$?
echo "Exist Code: ${exit_code}"
if [ ${exit_code} -ne 0 ]
then
	echo "Script error"
	exit 1
fi
	
python compare-bulk-data-sel.py --nameone ${var_name_one} --nametwo ${var_name_two} --file ${var_json_file} --table ${var_table}
exit_code=$?
echo "Exist Code: ${exit_code}"
if [ ${exit_code} -ne 0 ]
then
	echo "Script error"
	exit 1
fi

chmod 755 aws-upload-bulk-data-sel.sh

./aws-upload-bulk-data-sel.sh ${var_access_key} ${var_secret_key} ${var_name_one} ${var_name_two}
exit_code=$?
echo "Exist Code: ${exit_code}"
if [ ${exit_code} -ne 0 ]
then
	echo "Script error"
	exit 1
fi
