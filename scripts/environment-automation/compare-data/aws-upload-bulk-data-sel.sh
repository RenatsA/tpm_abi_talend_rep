#!/bin/sh

s3Key=${1}
s3Secret=${2}
var_name_one=${3}
var_name_two=${4}
dateFolder=`date '+%Y_%m_%d_%H_%M'`

function puts3 {
	file=$1
	folder=$2
	bucket=tpm-test-data
	resource="/${bucket}/data_audit/${dateFolder}_${var_name_one}_${var_name_two}/${folder}/${file}"
	contentType="text/plain"
	dateValue=`date -R`
	stringToSign="PUT\n\n${contentType}\n${dateValue}\n${resource}"

	signature=`echo -en ${stringToSign} | openssl sha1 -hmac ${s3Secret} -binary | base64`
	curl -s -X PUT -T "${file}" \
	  -H "Host: ${bucket}.s3.amazonaws.com" \
	  -H "Date: ${dateValue}" \
	  -H "Content-Type: ${contentType}" \
	  -H "Authorization: AWS ${s3Key}:${signature}" \
	  https://${bucket}.s3.amazonaws.com/data_audit/${dateFolder}_${var_name_one}_${var_name_two}/${folder}/${file}
}

if [ `find . -name '*summary*.csv' | wc -l` -gt 0 ];then
for i in `ls *summary*.csv`
do
	puts3 ${i} "summary"
done
fi

if [ `find . -name '*differences*.csv' | wc -l` -gt 0 ];then
for i in `ls *differences*.csv`
do
	puts3 ${i} "differences"
done
fi

if [ `find . -name '*.csv' | wc -l` -gt 0 ];then
for i in `ls *.csv`
do
	puts3 ${i} "export"
done
fi

echo "#######################################################################"
echo "#######################################################################"
echo "#######################################################################"
cat *summary*.csv
echo "#######################################################################"
echo "#######################################################################"
echo "#######################################################################"
