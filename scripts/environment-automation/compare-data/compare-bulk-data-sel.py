import os
import sys
import urllib
import urllib2
import json
import pprint
import requests
import logging
import collections
import getopt
import fnmatch
import re
import datetime
import argparse
import time
from xml.etree import ElementTree
import codecs
import base64
import os

sys.path.insert(0, '../../libs')
import SoapUtils

apiVersion = "41.0"

class BulkJob():
    def __init__(self,objectName,query):
        self.objectName=objectName
        self.query=query

def compareData():
    bulkOperation = "query"
    global bulkJob
    global diffcheck
    global countDifferences
    diffcheck = "false"

    fileNameOutputReport = time.strftime("%Y%m%d")  + "-" + "summary" + "-" + orgnameone + "-" + orgnametwo + ".csv"
    ##Open the file to write the output report to
    file_output_report = open(fileNameOutputReport,'w')
    file_output_report.write("Environments Compared: " + orgnameone + " - VS - " + orgnametwo)
    file_output_report.write("\n")
    file_output_report.write("#######################################################\n")
    file_output_report.write("Environments Compared:" + ",Object Compared: " + ", Number of Differences: " + "\n")
    
        
    for bulkJob in bulkJobs:
        logger.info("Comparing data for = " + bulkJob.objectName)
        fileNameOne = time.strftime("%Y%m%d")  + "-" + bulkJob.objectName + "-" + orgnameone + ".csv"
        fileNameTwo = time.strftime("%Y%m%d")  + "-" + bulkJob.objectName + "-" + orgnametwo + ".csv"
        fileNameOutput = time.strftime("%Y%m%d")  + "-" + "differences" + "-" + bulkJob.objectName + "-" + orgnameone + "-" + orgnametwo + ".csv"
       
        ##Open the file to write the output to
        file_output = open(fileNameOutput,'w')
        file_output.write("Files to be compared = " + fileNameOne + " - VS - " + fileNameTwo)
        file_output.write("\n")
        file_output.write("#######################################################\n")
        
        comparefiles(fileNameOne, fileNameTwo, fileNameOutput, file_output)
        
        file_output.write("\n")
        file_output.write("#######################################################\n")
        file_output.write("#######################################################\n")
        file_output.write("Files to be compared = " + fileNameTwo + " - VS - " + fileNameOne)
        file_output.write("\n")
        file_output.write("#######################################################\n")

        comparefiles(fileNameTwo, fileNameOne, fileNameOutput, file_output)

        file_output.write("\n")
        file_output.write("#######################################################\n")
        file_output.write("#######################################################\n")
        file_output.write("#######################################################\n")
        file_output.write("#######################################################\n")
        
        file_output.close()

        #Summary Report
        reportDifferences(fileNameOutput)
        file_output_report.write(orgnameone + " - VS - " + orgnametwo + "," + bulkJob.objectName + "," + str(countDifferences) + "\n")
        
        if diffcheck == "false":
            logger.info("No differences - removing file: " + fileNameOutput)
            os.remove(fileNameOutput)
        else:
            diffcheck = "false"

        logger.info("")

    file_output_report.close()


def comparefiles(fileNameOne, fileNameTwo, fileNameOutput, file_output):
    global diffcheck
	
    logger.info("Files to be compared = " + fileNameOne + " - VS - " + fileNameTwo)
    logger.info("Differences to be put into file = " + fileNameOutput)
        
    ## Open the file with read only permit
    file_one = open(fileNameOne)

    ## Read the first line 
    line_one = file_one.readline()

    # Initialize counter for line number
    line_no = 1

    #Print the header
    file_output.write("header,LineNumber=" + str(line_no) + "," + line_one)

    ## If the file is not empty keep reading line one at a time
    ## till the file is empty
    while line_one:

        line_match = "false"
            
        ## Open the file with read only permit
        file_two = open(fileNameTwo)
            
        #Read the first line
        line_two = file_two.readline()

        #Remove carriage return as some records will not have this
        line_one = line_one.strip('\n\r')

        while line_two:
            #Remove carriage return as some records will not have this
            line_two = line_two.strip('\n\r')

            if line_one == line_two:
                line_match = "true"
                #print("-----" + line_one + "--------" + line_two + "-----\n")

            line_two = file_two.readline()
            
        file_two.close()
        
        if line_match == "false":
            file_output.write("Different,LineNumber=" + str(line_no) + "," + line_one + "\n")
            diffcheck = "true"

#        if line_match == "true":
#            #print("Same,", line_one)
#            do_nothing="true"
#        else:
#            file_output.write("Different,LineNumber=" + str(line_no) + "," + line_one + "\n")
#            diffcheck = "true"
            #print("Different,", line_one)

        #Read next line
        line_one = file_one.readline()
        #increment line number
        line_no += 1

    file_one.close()

def reportDifferences(reportFile):
    global countDifferences
    countDifferences = 0
    
    ## Open the file with read only permit
    file_one = open(reportFile)

    ## Read the first line 
    line_one = file_one.readline()

    # Initialize counter for line number
    line_no = 1
 
    ## If the file is not empty keep reading line one at a time
    ## till the file is empty
    while line_one:

        if line_one.find("Different,LineNumber=") <> -1:
            countDifferences += 1
            #logger.info(line_one)
            #logger.info(countDifferences)

        #Read next line
        line_one = file_one.readline()
        #increment line number
        line_no += 1

    file_one.close()

def readObjects():
    global bulkJobs
    bulkJobs=[]
    logger.info("Reading Objects to export into memory.")
    json_data = json.load(open(objects))

    if(json_data):
        if(table == "ALL"):
            for obj in json_data['Objects']['types']:
                bulkJob = BulkJob(obj['name'],obj['query'])
                bulkJobs.append(bulkJob)
        else:
            for obj in json_data['Objects']['types']:
                if(obj['name'] == table):
                    bulkJob = BulkJob(obj['name'],obj['query'])
                    logger.info("Selected table = " + bulkJob.objectName)
                    bulkJobs.append(bulkJob)

        if(len(bulkJobs) > 0):
            logger.info("Objects to export stored in memory = " + str(len(bulkJobs)))
        else:
            logger.error("Failed to save list of Objects to export into memory.")
            sys.exit(1)
    else:
        logger.error("Failed to parse JSON data.")
        sys.exit(1)

def initialise_logger():
        # create logger
    logger = logging.getLogger('logger')

    logger.setLevel(logging.DEBUG)
    # create console handler and set level to debug
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)
    # create formatter
    formatter = logging.Formatter("%(asctime)s:%(levelname)s > %(message)s",
                              "%Y-%m-%d %H:%M:%S")
    # add formatter to ch
    ch.setFormatter(formatter)

    # add ch to logger
    logger.addHandler(ch)

    return logger
    
def main():
    global logger, objects, orgnameone, orgnametwo, table
    logger=initialise_logger()

    logger.info("Compares Metadata from SF Org.")
    parser = argparse.ArgumentParser(description='Compare record data from SF Org.')
    parser.add_argument("--nameone", type=str, help='Specify the environment Id of the SF Org.')
    parser.add_argument("--nametwo", type=str, help='Specify the environment Id of the SF Org.')
    parser.add_argument("--file", type=str, help='Specify the export objects.json file.')
    parser.add_argument("--table", type=str, help='Specify the table to query.')
    
    args = parser.parse_args()

    if (args.nameone is not None and args.nametwo is not None and args.file is not None and args.table is not None):
    
        objects = args.file
        orgnameone = args.nameone
        orgnametwo = args.nametwo
        table = args.table

        if(not os.path.exists(objects)):
            logger.error("Failed to locate " + objects)
            logger.error("Verify the path.")
            sys.exit(1)

        readObjects()
            
        compareData()

    else:
        parser.print_help()
        sys.exit(1)

if __name__ == '__main__':
    main()
