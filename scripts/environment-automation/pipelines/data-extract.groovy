#!groovy
RECIPIENTS_START="<EMAIL>@<ORG>.com"
RECIPIENTS_FINISH="<EMAIL>@<ORG>.com"
TECH="SF"


def repositoryCommiterEmail = '<EMAIL>@<ORG>.com'
def repositoryCommiterUsername = 'Jenkins'

pipeline {
    agent {label 'docker-large'}

    tools {
        jdk 'java8'
        ant 'ant-1.9.7'
    }
    options {
        // Only keep the 10 most recent builds
        buildDiscarder(logRotator(numToKeepStr:'10'))
    }

    parameters {
        string(defaultValue: 'DEV', description: 'Salesforce Environment.', name: 'SF_SERVER')
        string(defaultValue: 'EXPORT', description: 'Do not change this parameter.', name: 'TALEND_IMPORT_EXPORT')
        string(defaultValue: 'true', description: 'Do not change this parameter.', name: 'GIT_COMMIT_USAGE')
        string(defaultValue: 'false', description: 'If AWS is used.', name: 'AWS_USAGE')
        //DATA FOLDER IN BITBUCKET
        string(defaultValue: 'release', description: 'Do not change this parameter.', name: 'RELEASE_DATA_FOLDER')
        //ENV
        string(defaultValue: 'https://abinbev-ei-crm--tpmunittes.cs89.my.salesforce.com', description: 'Salesforce Login URL', name: 'SF_URL')
        string(defaultValue: 'd407d6f2-dd63-4b3d-a8cc-db7b70db9207', description: 'Salesforce Credentials from Jenkins', name: 'SF_CRED_ID')
        string(defaultValue: 'a8866c33-c1c2-4baf-874f-58458e584836', description: 'Bitbucket Credentials from Jenkins', name: 'GIT_CRED_ID')
        string(defaultValue: 'JrRCU3SFE2jcxdvBnuCvAXxCH', description: 'Salesforce Security Token from Jenkins', name: 'SF_TOKEN_ID')
        //REST OF THE PARAMETERS
        string(defaultValue: '', description: 'AWS Credential ID (not used)', name: 'AWS_CRED_ID')
        string(defaultValue: 'Main_General', description: 'Talend Artifact Name: Do not change this parameter.', name: 'TALEND_ARTIFACT')
        string(defaultValue: '0.1', description: 'Talend Artifact Version: Do not change this parameter.', name: 'TALEND_ARTIFACT_VERSION')
        //JOB LISTS
        string(defaultValue: 'ACCL__KPI_Set__c_EXP', description: 'Please type in the names of the jobs you want to execute. The list will be delimited by spaces.', name: 'SELECTED_TEMPLATES')
    }   
    stages {
        stage('TPM DATA CODE : SCM') {
            steps {
                //notifyStarted('STARTED','${JOB_BASE_NAME}')
                echo 'Building..'
                checkout([$class: 'GitSCM', 
                    branches: [[name: 'refs/remotes/origin/master']], 
                    userRemoteConfigs: [[credentialsId: "${params.GIT_CRED_ID}", 
                    url: 'https://arian.fetahaj@innersource.accenture.com/scm/projehtg/tpm_abi_talend_rep.git',
                    name: 'origin']],
                    extensions: [[$class: 'AuthorInChangelog'],
                    [$class: 'CheckoutOption', timeout: 20],
                    [$class: 'CleanBeforeCheckout'],
                    [$class: 'CloneOption',
                    depth: 0,
                    honorRefspec: false,
                    noTags: true,
                    //reference: '/var/tmp/gitcaches/<GIT-REPOSITORY>.reference',
                    shallow: true,
                    timeout: 3]
                    //[$class: 'RelativeTargetDirectory', relativeTargetDir: 'scripts']
                    ]])
            }
        }
        stage('TPM: SCM') {
            steps {
                echo "Cloning Repository"
				withCredentials([
					[$class: 'UsernamePasswordMultiBinding', credentialsId: "${params.GIT_CRED_ID}", usernameVariable: 'STASH_USERNAME', passwordVariable: 'STASH_PASSWORD']
				]) {
				script {
						sh "mkdir -p data; cd data; git clone --depth 1 https://${STASH_USERNAME}:${STASH_PASSWORD}@innersource.accenture.com/scm/projehtg/tpm_abi_talend_rep.git -b master;"
					}
                /*script {
                            sh "ls -la ${pwd()}"
                    }*/
                }
				echo "TPM: Cloned Repository."
				
            }
        }
        stage('TPM: Data Transport') {
            steps {
                echo "Data Deployment started..."
				withCredentials([
					[$class: 'UsernamePasswordMultiBinding', credentialsId: "${params.SF_CRED_ID}", usernameVariable: 'SF_USERNAME', passwordVariable: 'SF_PASSWORD'],
					[$class: 'UsernamePasswordMultiBinding', credentialsId: "${params.GIT_CRED_ID}", usernameVariable: 'STASH_USERNAME', passwordVariable: 'STASH_PASSWORD']
				]) {
				/*script {
                        sh 'cd scripts;\\"$(ls)\\";'
                    }*/
                script {
						sh "git config --global user.name 'Jenkins'; git config --global user.email 'arian.fetahaj@accenture.com'; echo ${params.SELECTED_TEMPLATES} > selected_templates.cfg; chmod 755 ./scripts/environment-automation/talend-scripts/talend.sh; ./scripts/environment-automation/talend-scripts/talend.sh '${SF_USERNAME}' '${SF_PASSWORD}' '${params.SF_TOKEN_ID}' '${params.SF_URL}' '${params.SF_SERVER}' '${params.TALEND_ARTIFACT}' '${params.TALEND_ARTIFACT_VERSION}' '${params.WORKSPACE}' '${params.TALEND_IMPORT_EXPORT}' '${params.GIT_COMMIT_USAGE}' 'https://${STASH_USERNAME}:${STASH_PASSWORD}@innersource.accenture.com/scm/projehtg/tpm_abi_talend_rep.git' '${params.RELEASE_DATA_FOLDER}';"
					}
				}
				echo "TPM: Data Deployment finished."
				
            }
        }
        
	}
    post {
        always {
            echo "Send notifications for result: ${currentBuild.result}"
            deleteDir()
            //notifyFinished('SUCCEEDED','${JOB_BASE_NAME}')
        }
        failure {
			echo "Post step Failure"
            //notifyFinished('FAILED','${JOB_BASE_NAME}')
        }
    }
}

