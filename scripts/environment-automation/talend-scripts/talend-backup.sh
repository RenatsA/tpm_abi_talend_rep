#!/bin/bash

if [ $# -eq 0 ]
	then
		echo "TPM: NO ARGUMENTS SUPPLIED."
elif [ $# -eq 12 ]
	then
		echo "TPM: PARAMETERS SUPPLIED."

		#Salesforce Credentials
		sf_username="${1}"
		sf_password="${2}"
        sf_securitytoken="${3}"
		sf_domain="${4}"
		sf_server="${5}"
		talend_job="${6}"
		talend_job_lower=`echo "${talend_job,,}"`
		talend_job_version="${7}"
		artifact="${talend_job}_${talend_job_version}.zip"
		WORKSPACE="${8}"
		talend_import_export="${9}"
		git_commit_usage="${10}"
		git_url="${11}"
		release_data_folder="${12}"

		echo "sf_username": ${sf_username}
		echo "sf_password": ${sf_password}
        echo "sf_securitytoken": ${sf_securitytoken}
		echo "sf_domain": ${sf_domain}
		echo "sf_server": ${sf_server}
		echo "talend_job": ${talend_job}
		echo "talend_job_lower": ${talend_job_lower}
		echo "talend_job_version": ${talend_job_version}
		echo "artifact": ${artifact}
		echo "WORKSPACE": ${WORKSPACE}
		echo "talend_import_export": ${talend_import_export}
		echo "git_commit_usage": ${git_commit_usage}
		echo "git_url": ${git_url}
		echo "release_data_folder": ${release_data_folder}

else
	echo "ERROR > INCORRECT PARAMETERS SUPPLIED."

	echo "sf_username": ${1}
	echo "sf_password": ${2}
    echo "sf_securitytoken": ${3}
	echo "sf_domain": ${4}
	echo "sf_server": ${5}
	echo "talend_job": ${6}
	echo "talend_job_version": ${7}
	echo "WORKSPACE": ${8}
	echo "talend_import_export": ${9}
	echo "git_commit_usage": ${10}
	echo "git_url": ${11}
	echo "release_data_folder": ${12}

	exit 1
fi

#Invalid password characters not supported by Talend
if [ `echo "${sf_password}" | grep "&" | wc -l` -gt 0 ]; then
	echo "TPM ERROR: PASSWORD CONTAINS INVALID CHARACTERS."
	exit 1
fi

WORKSPACE=`pwd`
echo "Workspace $WORKSPACE"

sf_api_version="/services/Soap/u/39.0"
sf_api_version="/services/Soap/u/39.0"
endpoint="${sf_domain}${sf_api_version}"

#talend_job="Main_General"
talend_project="tpm_abi"
export_location="${WORKSPACE}/scripts/environment-automation/exports/"
configuration_file_location="${WORKSPACE}/scripts/environment-automation/talend-build/build/"
checkin_export_location="${WORKSPACE}/data/tpm_abi_talend_rep/data/${release_data_folder}/"
local_export_location="${export_location}data_export/Business_Templates"
import_folder="${WORKSPACE}/data/${release_data_folder}/"
mkdir -p ${checkin_export_location}

selected_templates=`head -1 selected_templates.cfg`
echo "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
echo "TPM INFO: JOB LIST"
echo "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
echo " Selected Templates: ${selected_templates}"

echo "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"

old_IFS=$IFS
export IFS=","

for template in $sf_template
do
	selected_templates+=($template)
done
IFS=${old_IFS}

if [ ${#selected_templates[@]} -eq 0 ]; then
	echo "TPM ERROR: NO OBJECTS SELECTED."
	exit 1
else
	echo "TPM INFO: OBJECTS SELECTED."

	mkdir ${configuration_file_location}
	echo "SFObject;Skip;Condition" > "${configuration_file_location}/configuration.csv"

	for template in ${selected_templates[@]}
	do
		echo "TPM INFO: OBJECT > $template"
		echo "${template};" >> "${configuration_file_location}/configuration.csv"
	done

	echo "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
	cat ${configuration_file_location}/configuration.csv
	echo "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"

fi

echo "TPM INFO: JOB LIST."
echo "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"

generate_build_file(){
case $1 in
	"general")
		  cat > "${WORKSPACE}/scripts/environment-automation/talend-build/$talend_job/build.xml" << EOF

		  <project>
		  <property name="lib" location="../lib/*" />
		  <property name="job" location="${talend_job_lower}_0_1.jar" />


		  <target name="general">
				  <java classname="${talend_project}.${talend_job_lower}_0_1.${talend_job}" classpath="\${lib}:\${job}" fork="true" maxmemory="1024m">
					<arg line="--context_param sf_username=${sf_username}" />
					<arg line="--context_param sf_password=${sf_password}" />
					<arg line="--context_param sf_securitytoken=${sf_securitytoken}" />
					<arg line="--context_param export_file_location=${export_location}" />
					<arg line="--context_param sf_domain=${sf_domain}" />
					<arg line="--context_param sf_api_version=${sf_api_version}" />
					<arg line="--context_param endpoint=${endpoint}" />
					<arg line="--context_param configuration_file_location=${configuration_file_location}"/>
				  </java>
		  </target>
		  </project>
EOF
	;;
esac
}

echo "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
echo "TPM INFO:  TALEND OBJECT EXPORT."
echo "TPM INFO: RUNNING JOBS ON SELECTED OBJECTS."
#echo "INFO > Talend Org Context Property file = $talend_context_prop"
echo "TPM INFO: PROCESSING LATEST JOB BUILD = $artifact"
echo "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"

cd ${WORKSPACE}/scripts/environment-automation/talend-build

if [ $? -eq 0 ] && [ -f $artifact ]; then

	if [ -f $artifact ]; then
		unzip -q $artifact
		#rm -f $artifact

		cd ../../..
		generate_build_file "general"
		# generate the configuration.csv file in the build dir,so that the job class picks up only the selected templates

		logdate=$(date +%d-%m-%Y_%H:%M:%S)
		touch ${logdate}.txt
		# generate date for logs / backup

		mkdir -p ${checkin_export_location}${sf_server}/${logdate}/

		echo "TPM INFO: RUNNING JOB."

		#################################################################
		#### Copy files from cloned branch to local workspace
		#################################################################
		if [ ${git_commit_usage} == "true" ]; then
			echo "################################"
			cd ${WORKSPACE}
			mkdir -p scripts/environment-automation/exports/data_export/Business_Templates
			cp ${import_folder}*.csv ${export_location}
			echo "TPM INFO: FILES FOUND ON LOCAL FILESYSTEM AND TO BE USED BY TALEND:"
			ls -ltr ${export_location}
			echo "################################"
		else
			echo "TPM GIT: NO COMMITS MADE, GIT STATUS: ${git_commit_usage}"
			echo "################################"
		fi

		#################################################################
		#### Execute Ant
		#### Pipe output to a file
		#### Search the output file
		#################################################################
		cd ${WORKSPACE}/scripts/environment-automation/talend-build/$talend_job/
		ant -version
		ant general 2>&1 | tee ant_output_log.log

		#################################################################
		#### Copy files into cloned branch
		#### Push changes to stash
		#################################################################
		echo "################################"
		if [ ${talend_import_export} == "EXPORT" ]; then
			echo "GIT INFO: EXPORT JOB FILES TO BE PUSHED TO BITBUCKET."
			if [ `ls ${export_location}*.csv | wc -l` -ne 0 ]; then
				for i in `ls ${export_location}*.csv`
				do
					echo "GIT INFO: MOVE TO BITBUCKET: ${i}"
					cp ${i} ${checkin_export_location}${sf_server}/${logdate}/
				done

				if [ ${git_commit_usage} == "true" ]; then
					echo "GIT INFO: ATTEMPTING COMMIT, GIT STATUS: ${git_commit_usage}"
					cd ${checkin_export_location}${sf_server}/${logdate}/
					git status

					for i in `ls *.csv`
					do
						git add ${i}
					done
					git commit -m "Jenkins Backup from ${sf_domain}"
					git push ${git_url}

				else
					echo "GIT INFO: NO COMMITS MADE, GIT STATUS: ${git_commit_usage}"
				fi
			fi
		else
			echo "GIT INFO: IMPORT JOB, SO NO COMMITS TO BITBUCKET."
		fi

		#################################################################
		## PUSH LOGFILE TO BITBUCKET ##

		cp ${WORKSPACE}/*.txt ${WORKSPACE}/data/tpm_abi_talend_rep/info/
		cd ${WORKSPACE}/data/tpm_abi_talend_rep/info/
		git add *.txt
		git commit -m "Log for ${talend_import_export} from ${sf_domain}"
		git push ${git_url}
		echo "TPM INFO: LOG CREATED AND PUSHED TO BITBUCKET INFO FOLDER."

		#################################################################
		#### Error reporting of Talend job
		#################################################################
		echo "################################"
		cd ${WORKSPACE}/scripts/environment-automation/talend-build/$talend_job/
		if [ `grep "ERROR" ant_output_log.log | wc -l` -eq 0 ]; then
			echo "TPM INFO: TALEND JOB SUCCESSFUL."
		else
			echo "TPM ERROR: TALEND JOB FAILED."
			exit 1
		fi
		echo "" > ant_output_log.log

	else
		echo "TPM ERROR: BUILD $artifact DOES NOT EXIST."
		exit 1
	fi
else
	echo "TPM ERROR: BUILD $artifact DOES NOT EXIST."
	exit 1
fi